#! /usr/bin/env python

import cv2
import time
from absl import app, flags, logging
from absl.flags import FLAGS
import numpy as np
import tensorflow as tf
from models import (
    YoloV3, YoloV3Tiny
)
from dataset import transform_images, load_tfrecord_dataset
from utils import draw_outputs

from nursie_yolo.srv import YoloService, YoloServiceResponse
import rospy

# We do this because of a bug in kinetic when importing cv2
import sys
#import subprocess
#new_proc = subprocess.Popen(["rosversion", "-d"], stdout=subprocess.PIPE)
#version_str = new_proc.communicate()[0]
#ros_version = version_str.decode('utf8').split("\n")[0]
ros_version = "melodic"
# if ros_version == "kinetic":
#     try:
#         sys.path.remove('/opt/ros/kinetic/lib/python2.7/dist-packages')
#     except Exception as ex:
#         print(ex)
#         print("Its already removed..../opt/ros/kinetic/lib/python2.7/dist-packages")


flags.DEFINE_string(
    'classes', '/home/lussi/nursie_ws/src/nursie_yolo/src/new_names.names', 'path to classes file')
flags.DEFINE_string('weights', '/home/lussi/nursie_ws/src/nursie_yolo/checkpoints/yolov3_train_20.tf',
                    'path to weights file')
flags.DEFINE_boolean('tiny', False, 'yolov3 or yolov3-tiny')
flags.DEFINE_integer('size', 416, 'resize images to')
flags.DEFINE_string(
    'image', '/home/lussi/nursie_ws/src/nursie_yolo/images/test06.jpeg', 'path to input image')
flags.DEFINE_string('tfrecord', None, 'tfrecord instead of image')
flags.DEFINE_string(
    'output', '/home/lussi/nursie_ws/src/nursie_yolo/output/output_06.png', 'path to output image')
flags.DEFINE_integer('num_classes', 2, 'number of classes in the model')


def reconocimiento(_argv):
    print("Imagen recibida")
    physical_devices = tf.config.experimental.list_physical_devices('GPU')
    if len(physical_devices) > 0:
        tf.config.experimental.set_memory_growth(physical_devices[0], True)

    if FLAGS.tiny:
        yolo = YoloV3Tiny(classes=FLAGS.num_classes)
    else:
        yolo = YoloV3(classes=FLAGS.num_classes)

    yolo.load_weights(FLAGS.weights).expect_partial()
    logging.info('weights loaded')

    class_names = [c.strip() for c in open(FLAGS.classes).readlines()]
    logging.info('classes loaded')

    if FLAGS.tfrecord:
        dataset = load_tfrecord_dataset(
            FLAGS.tfrecord, FLAGS.classes, FLAGS.size)
        dataset = dataset.shuffle(512)
        img_raw, _label = next(iter(dataset.take(1)))
    else:
        img_raw = tf.image.decode_image(
            open(FLAGS.image, 'rb').read(), channels=3)

    img = tf.expand_dims(img_raw, 0)
    img = transform_images(img, FLAGS.size)

    t1 = time.time()
    boxes, scores, classes, nums = yolo(img)
    t2 = time.time()
    logging.info('time: {}'.format(t2 - t1))

    logging.info('detections:')
    for i in range(nums[0]):
        logging.info('\t{}, {}, {}'.format(class_names[int(classes[0][i])],
                                           np.array(scores[0][i]),
                                           np.array(boxes[0][i])))

    img = cv2.cvtColor(img_raw.numpy(), cv2.COLOR_RGB2BGR)
    img = draw_outputs(img, (boxes, scores, classes, nums), class_names)
    cv2.imwrite(FLAGS.output, img)
    logging.info('output saved to: {}'.format(FLAGS.output))
    print("Devolviendo respuesta")
    return "Pepito"


def reconocimiento_server():
    rospy.init_node('reconocimiento_server')
    s = rospy.Service('reconocimiento', YoloService, reconocimiento)
    print("Listo para recibir imágenes.")
    rospy.spin()


if __name__ == '__main__':
    try:
        reconocimiento_server()
    except SystemExit:
        pass
